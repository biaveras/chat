package cliente_servidor;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Runnable {

	//variaveis static sao visiveis em todas as threads
	//armazena o socket conectado com o servidor
	static Socket cliente;

	//armazena o caminho do arquivo
	public static String file;

	public Client(Socket cliente){
		this.cliente = cliente;
	}

	public static void main (String[] args) throws IOException {

		cliente = new Socket("127.0.0.1", 8080);

		//conecta no servidor com o novo socket
		PrintStream saida = new PrintStream(cliente.getOutputStream());
		Scanner teclado = new Scanner(System.in);

		//abre uma thread para o metodo run() com a finalidade de ter entrada e saida de informacoes ao mesmo tempo
		Thread t = new Thread(new Client(cliente));
		t.start();

		//se o cliente enviar qualquer mensagem, o metoto .hasNextLine() sera liberado
		while (teclado.hasNextLine()) {
			String message = teclado.nextLine();

			//finaliza sessao
			if(message.contains("exit")) {
				System.out.println("Sua sessao foi finalizada");
				System.exit(0);
			}

			//a troca de informacoes entre cliente e servidor no envio do arquivo nao sera exibida na tela.
			if(message.toUpperCase().startsWith("/ENVIARARQUIVO")) {
				String comando = verificarArquivo(message);
				if(comando != null) {

					//envia mensagem ao servidor depois da verificacao
					saida.println(comando);
				} else {
					System.out.println("Arquivo nao encontrado");                	
				}

				//fluxo normal de envio de mensagens ao servidor
			} else {
				saida.println(message);
			}
		}
	}

	//thread que tem a funcao de receber mensagens do servidor
	@Override
	public void run() {
		try {
			Scanner entrada = new Scanner(this.cliente.getInputStream());

			//se o cliente receber qualquer mensagem, o metoto .hasNextLine() sera liberado
			while(entrada.hasNextLine()){
				String message = entrada.nextLine();

				//o servidor consegue executar alguns comandos no cliente, se a mensagem comecar com barra "/", o client executa o comando
				if (!message.startsWith("/")) {
					
					//se nao tiver barra "/", printa a mensagem vinda do servidor para o usuario
					System.out.println(message);
				} else {

					//divide os parametros do comando
					String[] splitted = message.split(":");
					switch (splitted[0]) {

					//client de origem envia o arquivo
					case "/ip":
						this.enviarArquivo(splitted[1].replace("/", ""), splitted[2]);
						break;

						//client de destino abre o servidor
					case "/abrirserv":
						this.abrirServidorArquivos(Integer.decode(splitted[1]), splitted[2]);
						break;
					default:
						break;
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//metodo que verifica se o arquivo existe antes de enviar ao servidor
	public static String verificarArquivo(String message) {
		String[] caminho = message.split(" ");

		File f = new File(caminho[1]);

		if (f.exists()) {
			file = caminho[1];
			return caminho[0];
		} else {
			return null;
		}
	}

	//caso esse seja o client de destino, abre um servidor temporario para receber o arquivo
	//recebe qual a porta deve abrir e o nome de quem esta enviando o arquivo
	public void abrirServidorArquivos(int port, String origem) {
		try {

			ServerSocket fServ = new ServerSocket(port);

			//espera a conexao do client de origem
			Socket fSock = fServ.accept();
			InputStream in = fSock.getInputStream();

			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader reader = new BufferedReader(isr);
			String fName = reader.readLine();
			String user = System.getProperty("user.home");

			//cria o arquivo nos downloads
			File f1 = new File(user+"\\downloads\\" + fName);
			FileOutputStream out = new FileOutputStream(f1);

			//salva o conteudo em bytes recebido do socket na variavel b
			byte[] b = new byte[fSock.getReceiveBufferSize()];
			BufferedInputStream bf = new BufferedInputStream(in);
			bf.read(b);

			//escreve o arquivo no computador do client de destino e finaliza o socket
			out.write(b);
			out.flush();
			out.close();

			fServ.close();

			System.out.println("[CLIENT]: Arquivo recebido de " + origem);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	//metodo para enviar o arquivo, recebe o ip e porta do servidor temporario criado no usuario de destino
	public void enviarArquivo(String ip, String port) {
		try {
			File f = new File(file);
			FileInputStream in = new FileInputStream(f);

			//cria o socket com o endereco que o servidor mandou
			Socket socket = new Socket(ip, Integer.decode(port));

			OutputStream out = socket.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(out);
			BufferedWriter writer = new BufferedWriter(osw);
			writer.write(f.getName() + "\n");
			writer.flush();
			byte[] b = new byte[2000000];
			int lidos = -1; 

			//escreve o conteudo do arquivo dentro do outputStream e envia
			while ((lidos = in.read(b, 0, b.length)) != -1) {
				out.write(b, 0, lidos);
			}

			//apos toda a transferencia, printa ao usuario
			System.out.println("[CLIENT]: Arquivo enviado");
			socket.close();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}