package cliente_servidor;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;

public class Server implements Runnable {

	public String currentName;
	public Socket clienteAtual;
	public static Map<String, Socket> nomesServidor;
	public Scanner entrada;

	public Server(Socket cliente) {
		this.clienteAtual = cliente;
	}

	public static void main(String[] args) throws IOException {
		
		//inicia socket
		ServerSocket servidor = new ServerSocket(8080);
		System.out.println("Servidor iniciado na porta 8080");

		//cria uma lista para inserir os cliente
		nomesServidor = new HashMap<String, Socket>();

		while (true) {
			
			//espera um cliente se conectar
			Socket cliente = servidor.accept();
			Thread t = new Thread(new Server(cliente));
			t.start();
		}
	}

	@Override
	public void run() {

		try {
			this.entrada = new Scanner(clienteAtual.getInputStream());
			this.retornarMensagem("Para entrar, digite seu nickname (sem espaco): ");

			//verifica se o nome esta disponivel
			this.currentName = this.validarNick(null);

			//insere o cliente na lista com o nome e o socket
			nomesServidor.put(currentName, this.clienteAtual);

			System.out.println("Cliente " + clienteAtual.getRemoteSocketAddress() + " conectado com o apelido: " + currentName);
			this.retornarMensagem("Voce se conectou ao chat!\nPara listar os comandos do servidor, digite /ajuda");

			while (this.entrada.hasNextLine()) {
				String message = this.entrada.nextLine();

				//mensagens que comecam com / sao tratadas como comando
				if (message.startsWith("/")) {
					System.out.println(message);
					if (message.trim().contains(" ")) {
						
						//separa o comando da mensagem
						String[] splitedMessage = message.split(" ", 2);

						//trata os comandos especificamente
						this.executarComando(splitedMessage[0], splitedMessage[1]);
					} else {
						//trata os comandos especificamente
						this.executarComando(message, null);
					}
					continue;
				}

				//enviar mensagem para todos
				enviarMensagemParaTodos(message);
			}

			//caso nao receba mais mensagens/comandos ou o cliente finalize a conexao, o cliente sera desconectado
			if(nomesServidor.containsKey(this.currentName)){

				//remove usuario da lista
				nomesServidor.remove(this.currentName, this.clienteAtual);
				System.out.println("CONEXAO FINALIZADA - "+ currentName + ", " + clienteAtual.getRemoteSocketAddress());

				//envia mensagem para todos avisando que um cliente saiu
				for (Entry<String, Socket> item : nomesServidor.entrySet()) {
					try {
						PrintStream enviar = new PrintStream(item.getValue().getOutputStream());
						enviar.println("[ALERTA]: "+this.currentName + " saiu do chat!");
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}

			//fecha a conexao
			entrada.close();
			this.clienteAtual.close();

		} catch (IOException ex) {

		}
	}

	//metodo para tratar cada comando
	private void executarComando(String command, String message) {
		String parameter = null;

		//Caso tenha parametro, separa ele do comando principal
		if (command.contains(":")) {
			parameter = command.split(":")[1];
			command = command.split(":")[0];

			//verifica se a lista contem o usuario(parametro) informado
			if(!nomesServidor.containsKey(parameter)){
				retornarMensagem("[ALERTA]:  Usuario nao encontrado");
			}
		}

		switch (command.toUpperCase()) {
		case "/AJUDA":
			
			//comando para obter ajuda
			retornarMensagem(
					"- /privado:{NomeUsuario} Mensagem\t\t\t Ex: /privado:Ana Ola \n"
							+ "- /enviarArquivo:{NomeUsuario} {CaminhoArquivo}\t\t Ex: /enviarArquivo C:/Desktop/imagem.png \n"
							+ "- /mudarNick {nick}\t\t\t\t\t Ex: /mudarNick joaoBonitao");
			break;
		case "/PRIVADO":
			
			//verifica se foi informado o nome do cliente
			if (parameter != null) {
				
				//enviar mensagem privada
				this.enviarMensagemPrivada(message, parameter);
			} else {
				retornarMensagem("[ALERTA]:  Comando invalido. Obrigatorio informar o nome de usuario\nEx: /privado:EscrevaONome");
			}

			break;

			//cria uma porta randomica e envia para o client de origem e destino
		case "/ENVIARARQUIVO":
			if(parameter != null) {
				for (Entry<String, Socket> i : nomesServidor.entrySet()) {
					if (i.getKey().equals(parameter)) {
						try {
							PrintStream saida = new PrintStream(i.getValue().getOutputStream());

							//obtem uma porta randomica ate 9999
							Random ran = new Random();
							int port = ran.nextInt(10000); 

							//retorna o comando para o client processar e abrir o servidor com a porta criada
							saida.println("/abrirserv:"+ port + ":" + currentName);              	

							//retorna o comando para o client de origem processar e enviar o arquivo no ip do client de destino
							String addr = i.getValue().getRemoteSocketAddress().toString().split(":")[0];
							retornarMensagem("/ip:" + addr + ":" + port);
						} catch(Exception ex) {
							ex.printStackTrace();
						}

						break;
					}
				}
			}
			else {
				//enviar para todos
				retornarMensagem("[ALERTA]: Nao suportado");
				
			}

			break;
		case "/MUDARNICK":
			
			//comando para mudar de nick
			//grava o nome atual
			String nomeAntigo = this.currentName;

			//grava o nome que deseja mudar, apos a validacao
			String nomeTemp = this.validarNick(message);

			if (nomeTemp != null){
				nomesServidor.remove(nomeAntigo, this.clienteAtual);
				
				//atualiza o nome atual
				this.currentName = nomeTemp;
				
				//atualiza a lista com o nome informado
				nomesServidor.put(this.currentName, clienteAtual);

				//envia mensagem para todos informado que o cliente mudou de nickname
				for (Entry<String, Socket> item : nomesServidor.entrySet()) {
					try {
						PrintStream enviar = new PrintStream(item.getValue().getOutputStream());
						enviar.println("[ALERTA]: " + nomeAntigo + " mudou o nickname para " + this.currentName);
					} catch (IOException ex) {
						ex.printStackTrace();
					}
				}
			}
			break;
		default:
			retornarMensagem("[ALERTA]: Comando invalido, digite /ajuda para obter a lista de comandos.");
			break;
		}
	}

	//metodo para retornar mensagens para o cliente
	private void retornarMensagem(String mensagem) {
		try {
			PrintStream saida = new PrintStream(this.clienteAtual.getOutputStream());
			saida.println(mensagem);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	//metodo para enviar mensagem para um usuario especifico (privado)
	private void enviarMensagemPrivada(String message, String recipient) {
		try {
			for (Entry<String, Socket> i : nomesServidor.entrySet()) {
				
				//pega somente o usuario que foi informado para receber a mensagem
				if (i.getKey().equals(recipient)) {
					PrintStream saida = new PrintStream(i.getValue().getOutputStream());
					saida.println("[PRIVADO] " + currentName + ": " + message);
				}
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	//metodo para enviar mensagem para todos os clientes conectados
	public void enviarMensagemParaTodos(String mensagem) {
		
		//loop para pegar todos os clientes da lista
		for (Entry<String, Socket> item : nomesServidor.entrySet()) {
			if (!item.getKey().equals(currentName)) {
				try {
					PrintStream saida = new PrintStream(item.getValue().getOutputStream());
					
					//envia mensagem para todos os cliente, exceto o cliente que esta enviando a mensagem
					saida.println("[GERAL] " + currentName + ": " + mensagem);
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
	//comando para validar o nick que o cliente digitou
	public String validarNick(String nick) {
		String nome = nick;
		while(true){
			if(nome == null) {
				
				//recebe o nome digitado
				nome = entrada.nextLine();
			}

			//verifica se ja existe o nome na lista
			if (nomesServidor.containsKey(nome.trim())) {
				this.retornarMensagem("[ALERTA]: Este nickname ja esta sendo usado. Defina um novo: ");
			}
			
			//verifica se possue mais de um caracter
			else if (nome.trim().length() == 0) {
				this.retornarMensagem("[ALERTA]: Seu nick deve conter pelo menos um caracter");
			}
			
			//verificar se o nome contem espacos em branco
			else if (nome.trim().contains(" ")) {
				this.retornarMensagem("[ALERTA]: Seu nick nao pode ter espaco");
			}
			
			//comando para sair do metodo
			else if("/sair".equals(nome)){
				return null;
			}
			else {
				return nome.trim();
			}
			nome = null;
		}
	}

}
